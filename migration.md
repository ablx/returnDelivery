# Fragen
**Wo werden die Retouren erzeugt / wo startet der Prozess?**

*Direkt im Altsystem*
Die Funktionalität könnte dann durch den Aufruf des Lamdas ersetzt werden. 
Kann die Erzeugung der Retouren woanders erfolgen?

*In vorgelagerten System*
Wenn die Retouren von einem vorgelagerten System erzeugt werden könnte dieses ggf. die Lambda Funktion direkt aufrufen. 

**Wie werden die Folgesysteme vom Altsystem angesprochen?**
1. REST / SOAP über Http
2. Messaging (welches System?)
3. ...
4. Wie sieht die Datenstruktur aus?

**Wo liegen die Folgesysteme?**
1. OnPremise bei Otto (muss aus AWS auf die Systeme zugegriffen werden?)
2. In AWS -> dann könnten die Systeme direkt nach den Lambda aufgerufen werden (Message Queue)

**Hängen andere Systeme implizit von den Retourenmeldungen ab?**
1. Existieren DB-Trigger etc?
2. Rufen andere Anwendungen die Retourentabelle direkt ab?
3. Existieren Views, die die Retouren benötigen?

D.h. muss weiterhin in die Oracle DB geschrieben werden? Welches System übernimmt das?

## Migrationsansätze
Im Altsystem wird die Logik für die Validierung der Retouren entfernt und gegen einen Aufruf der neuen Lambda Funktion ersetzt. 
Das Altsystem leitet die Antwort dann wie bisher an die Folgesysteme weiter. 
* Wenig Codeänderungen
* Es ist egal, wo die Folgesysteme liegen und wie sie angesprochen werden
* Alte Datenstrukturen können/ müssen weiterbenutzt werden
* Es kann weiterhin in die Oracle DB geschrieben werden

Wenn die Folgesysteme schon in AWS liegen können diese durch die Lambda Funktion aufgerufen werden (bzw. asynchron über Messaging). 
Wenn die Retouren nicht im Altsystem erzeugt werden kann dieses komplett umgangen werden. 
