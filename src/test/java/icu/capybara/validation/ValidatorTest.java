package icu.capybara.validation;

import icu.capybara.api.LineItem;
import icu.capybara.api.Reason;
import icu.capybara.api.ReturnDeliveryRequest;
import org.junit.jupiter.api.*;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


class ValidatorTest {

    private final Validator validator = new Validator();
    private ReturnDeliveryRequest returnDeliveryRequest;
    private final LineItem validLineItem = LineItem.builder()
            .articleId("articleId")
            .comment("comment")
            .reason(Reason.DAMAGED)
            .build();


    @Nested
    @DisplayName("Given null")
    class GivenNull {


        @Test
        @DisplayName("then the validation should throw an exception")
        void thenTheValidationShouldSucceed() {
            assertThatThrownBy(() -> validator.validate(null)).isExactlyInstanceOf(IllegalArgumentException.class);
        }

    }


    @Nested
    @DisplayName("Given a valid return delivery object")
    class GivenAValidReturnDeliveryRequestWithAllMandatoryFields {
        private Result result;

        @BeforeEach
        void setUp() {

            returnDeliveryRequest = ReturnDeliveryRequest.builder()

                    .processStarted(new Date())
                    .lineItem(validLineItem)
                    .orderId("1")
                    .customerId("1")
                    .build();
            result = validator.validate(returnDeliveryRequest);
        }

        @Test
        @DisplayName("then the validation result should not be null")
        void thenTheResultShouldNotBeNull() {
            assertThat(result).isNotNull();
        }

        @Test
        @DisplayName("then the validation result should be valid")
        void thenTheValidationShouldSucceed() {
            assertThat(result.isValid()).isTrue();
        }


        @Test
        @DisplayName("then no descriptions of invalid fields should be present")
        void thenNoInvalidFieldDescriptionsShouldBePresent() {
            assertThat(result.getInvalidFields()).isEmpty();
        }

    }

    @Nested
    @DisplayName("Given a return delivery without line items")
    class GivenAReturnDeliveryDataWithNoLineItemsRequest {
        private Result result;

        @BeforeEach
        void setUp() {
            returnDeliveryRequest = ReturnDeliveryRequest.builder()
                    .customerId("1")
                    .orderId("1")
                    .processStarted(new Date())
                    .build();
            result = validator.validate(returnDeliveryRequest);
        }

        @Test
        @DisplayName("then the validation result should not be null")
        void thenTheResultShouldNotBeNull() {
            assertThat(result).isNotNull();
        }

        @Test
        @DisplayName("then the validation result should be invalid")
        void thenTheValidationShouldSuceed() {
            assertThat(result.isValid()).isFalse();
        }

        @Test
        @DisplayName("then a description for the invalid field is present")
        void thenNoInvalidFieldDescriptionsShouldBePresent() {
            assertThat(result.getInvalidFields()).isNotEmpty();
        }


    }


    @Nested
    @DisplayName("Given a invalid request with a valid line item")
    class GivenAReturnDeliveryDataWithoutIdButLineItemRequest {
        private Result result;

        @BeforeEach
        void setUp() {

            returnDeliveryRequest = ReturnDeliveryRequest.builder()
                    .lineItem(validLineItem)
                    .build();
            result = validator.validate(returnDeliveryRequest);
        }

        @Test
        @DisplayName("then the validation result should not be null")
        void thenTheResultShouldNotBeNull() {
            assertThat(result).isNotNull();
        }

        @Test
        @DisplayName("then the validation result should be invalid")
        void thenTheValidationShouldSucceed() {
            assertThat(result.isValid()).isFalse();
        }

        @Test
        @DisplayName("then a description for the invalid field is present")
        void thenNoInvalidFieldDescriptionsShouldBePresent() {
            assertThat(result.getInvalidFields()).isNotEmpty();
            assertThat(result.getInvalidFields().size()).isEqualTo(5);
        }

    }


}