package icu.capybara;

import com.amazonaws.services.dynamodbv2.document.Item;
import icu.capybara.api.LineItem;
import icu.capybara.api.Reason;
import icu.capybara.api.ReturnDeliveryRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;


class MapperTest {

    private final Mapper mapper = new Mapper();

    @Nested
    @DisplayName("Given ReturnDeliveryResponse")
    class GivenAnvalidRequestObject {

        private Item result;

        @BeforeEach
        void setUp() {
            result = mapper.returnDeliveryToItem(ReturnDeliveryRequest.builder()
                    .processStarted(new Date(0))
                    .processFinished(new Date(1))
                    .customerId("1")
                    .orderId("1")
                    .lineItem(LineItem.builder()
                            .reason(Reason.DAMAGED)
                            .comment("comment")
                            .articleId("1")
                            .build())
                    .build());
        }

        @Test
        @DisplayName("Then the mapper should map simple fields")
        void thenTheMapperShouldMapSimpleFields() {
            assertThat(result.get("customerId")).isNotNull().isEqualTo("1");
            assertThat(result.get("orderId")).isNotNull().isEqualTo("1");
        }

        @Test
        @DisplayName("Then the mapper should map line items")
        void thenTheMapperShouldMapLineItems() {
            List<Object> lineItems = result.getList("lineItems");
            Map<String, String> expected = new HashMap<>();
            expected.put("comment", "comment");
            expected.put("articleId", "1");
            expected.put("reason", "DAMAGED");

            assertThat(lineItems).isNotNull().containsExactly(
                    expected
            );
        }

        @Test
        @DisplayName("Then the mapper should format date fields")
        void thenTheMapperShouldFormatDateFields() {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            assertThat(result.get("processStarted")).isNotNull()
                    .isEqualTo(sdf.format(new Date(0)));

            assertThat(result.get("processFinished")).isNotNull()
                    .isEqualTo(sdf.format(new Date(1)));
        }
    }

}