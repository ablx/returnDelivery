package icu.capybara;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import icu.capybara.api.LineItem;
import icu.capybara.api.Reason;
import icu.capybara.api.ReturnDeliveryRequest;
import icu.capybara.api.ReturnDeliveryResponse;
import org.junit.jupiter.api.*;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Date;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Testcontainers
class ProcessReturnDeliveryRequestTest {

    @Container
    private static final LocalStackContainer LOCAL_STACK_CONTAINER = new LocalStackContainer()
            .withServices(LocalStackContainer.Service.DYNAMODB);
    private static Context CONTEXT;
    private static DynamoDB DB;

    @BeforeAll
    static void setUp() {
        CONTEXT = mock(Context.class);
        when(CONTEXT.getLogger()).thenReturn(mock(LambdaLogger.class));

        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(Region.getRegion(Regions.EU_CENTRAL_1));
        DB = new DynamoDB(client);

    }

    @Nested
    @DisplayName("Given a valid request object")
    class GivenAValidRequestObject {
        @Test
        @DisplayName("Then function should save the data to dynamo and return success.")
        void thenSaveToDynamoAndReturnSuccess() {

            ProcessReturnDelivery processReturnDelivery = new ProcessReturnDelivery();

            ReturnDeliveryRequest data = ReturnDeliveryRequest.builder()
                    .customerId("1")
                    .orderId("1")
                    .processStarted(new Date())
                    .lineItem(LineItem.builder().articleId("bar").reason(Reason.DAMAGED).build())
                    .build();

            ReturnDeliveryResponse resp = processReturnDelivery.handleRequest(data, CONTEXT);

            assertThat(resp).isNotNull();
            assertThat(resp.getMessage()).contains("Saved");
            assertThat(resp.isProcessed()).isTrue();
            assertThat(resp.getId()).isNotBlank();

            Table table = DB.getTable("ReturnDelivery");
            Item item = table.getItem("id", resp.getId());
            assertThat(item).isNotNull();
            assertThat(item.get("lineItems")).isNotNull();
            Long items = table.getDescription().getItemCount();
            assertThat(items).isEqualTo(1);
        }
    }
    @Nested
    @DisplayName("Given an invalid request object")
    class GivenAnInvalidRequestObject {
        @Test
        @DisplayName("Then the function should not save the data to dynamo db")
        void thenSaveToDynamoAndReturnSuccess() {

            ProcessReturnDelivery processReturnDelivery = new ProcessReturnDelivery();

            ReturnDeliveryRequest data = ReturnDeliveryRequest.builder()
                    .orderId("1")
                    .processStarted(new Date())
                    .lineItem(LineItem.builder().articleId("bar").reason(Reason.DAMAGED).build())
                    .build();

            ReturnDeliveryResponse resp = processReturnDelivery.handleRequest(data, CONTEXT);

            assertThat(resp).isNotNull();
            assertThat(resp.getMessage()).contains("customerId may not be null");
            assertThat(resp.isProcessed()).isFalse();
            assertThat(resp.getId()).isBlank();

            Long items = DB.getTable("ReturnDelivery").getDescription().getItemCount();
            assertThat(items).isEqualTo(0);
        }
    }
}
