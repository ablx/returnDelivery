package icu.capybara.validation;


import lombok.Getter;

import java.util.ArrayList;
import java.util.List;


public final class Result {

    @Getter
    private boolean valid;

    @Getter
    private final List<String> invalidFields = new ArrayList<>();

    Result(List<String> invalidFields) {
        this.invalidFields.addAll(invalidFields);
        this.valid = invalidFields.isEmpty();
    }

    public String getDescription() {
        return "Validation failed: " + String.join(", ", invalidFields);
    }
}
