package icu.capybara.validation;

import icu.capybara.api.LineItem;
import icu.capybara.api.ReturnDeliveryRequest;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.*;
import java.util.stream.Collectors;

public final class Validator {


    public Result validate(ReturnDeliveryRequest data) {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();
        Set<ConstraintViolation<ReturnDeliveryRequest>> violations = validator.validate(data);

        return new Result(violations.stream()
                .map(c -> c.getPropertyPath().toString() + ": " + c.getMessage())
                .collect(Collectors.toList()));
    }


}
