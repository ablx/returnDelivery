package icu.capybara.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public final class ReturnDeliveryResponse {

    private boolean processed;
    private String id;
    private String message;

}
