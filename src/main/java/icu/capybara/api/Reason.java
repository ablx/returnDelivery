package icu.capybara.api;

public enum Reason {

    TOO_SMALL, TOO_LARGE, DAMAGED, OTHER
}
