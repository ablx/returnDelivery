package icu.capybara.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public final class LineItem {

    @NotNull
    @NotBlank
    private String articleId;

    private String comment;

    @NotNull
    private Reason reason;


}
