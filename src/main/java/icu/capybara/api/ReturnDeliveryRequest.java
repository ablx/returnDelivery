package icu.capybara.api;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public final class ReturnDeliveryRequest {

    @NotNull
    @NotBlank
    private String customerId;

    @NotNull
    @NotBlank
    private String orderId;

    @NotNull
    @PastOrPresent
    private Date processStarted;
    private Date processFinished;

    @Singular
    @NotEmpty
    @NotNull
    private List<LineItem> lineItems;


}
