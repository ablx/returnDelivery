package icu.capybara;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import icu.capybara.api.ReturnDeliveryRequest;
import icu.capybara.api.ReturnDeliveryResponse;
import icu.capybara.validation.Result;
import icu.capybara.validation.Validator;


public class ProcessReturnDelivery {
    private final Validator validator = new Validator();

    private final Mapper mapper = new Mapper();

    private DynamoDB dynamoDB;

    public ProcessReturnDelivery() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(Region.getRegion(Regions.EU_CENTRAL_1));
        this.dynamoDB = new DynamoDB(client);
    }


    public ReturnDeliveryResponse handleRequest(ReturnDeliveryRequest data, Context context) {
        context.getLogger().log("Recieved " + data);
        Result result = validator.validate(data);
        if (result.isValid()) {
            context.getLogger().log("Data is valid. Saving to DB.");
            String id = save(data);

            return new ReturnDeliveryResponse(true, id, "Saved return delivery.");
        } else {
            context.getLogger().log("Data is invalid. " + result.getDescription());
            return new ReturnDeliveryResponse(false, null, result.getDescription());
        }


    }

    private String save(ReturnDeliveryRequest data) {
        Table returnDelivery = dynamoDB.getTable("ReturnDelivery");
        Item item = mapper.returnDeliveryToItem(data);
        returnDelivery.putItem(
                new PutItemSpec().withItem(item));
        return item.getString("id");
    }


}
