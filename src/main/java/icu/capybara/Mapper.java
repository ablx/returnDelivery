package icu.capybara;

import com.amazonaws.services.dynamodbv2.document.Item;
import icu.capybara.api.LineItem;
import icu.capybara.api.ReturnDeliveryRequest;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Map objects / json to DynamoDB Items.
 * Should be replaced with ORM.
 */
final class Mapper {

    final Item returnDeliveryToItem(ReturnDeliveryRequest request) {
        final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        Item item = new Item();
        item
                .withPrimaryKey("id", UUID.randomUUID().toString())
                .withString("customerId", request.getCustomerId())
                .withString("orderId", request.getOrderId())
                .withString("processStarted", sdf.format(request.getProcessStarted()))

                .withList("lineItems", request.getLineItems().stream()
                        .map(this::lineItemToMap).collect(Collectors.toList()));

        if (request.getProcessFinished() != null) {
            item.withString("processFinished", sdf.format(request.getProcessFinished()));
        }

        return item;
    }


    private Map<String, String> lineItemToMap(LineItem lineItem) {
        Map<String, String> map = new HashMap<>();
        map.put("articleId", lineItem.getArticleId());
        map.put("comment", lineItem.getComment());
        map.put("reason", lineItem.getReason().toString());
        return map;
    }


}
