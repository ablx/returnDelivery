#!/usr/bin/env bash

aws s3 rm s3://ablx.lambda/return-delivery-1.0.0.jar
aws lambda delete-function --function-name ReturnDelivery
aws dynamodb delete-table --table-name ReturnDelivery