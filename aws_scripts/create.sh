#!/usr/bin/env bash

# Create the bucket which holds the lambda code
bucket_name='ablx.lambda'
aws s3 mb s3://${bucket_name} && \
aws s3api put-bucket-versioning --bucket ${bucket_name} --versioning-configuration Status=Enabled

# Build and upload the code
mvn clean package shade:shade && \
aws s3 cp target/return-delivery-1.0.0.jar s3://${bucket_name}/return-delivery-1.0.0.jar && \

# Create the function
aws lambda create-function --cli-input-json file://aws_scripts/json/create_function.json && \

# Create the DB
aws dynamodb create-table --table-name ReturnDelivery \
 --attribute-definitions AttributeName=id,AttributeType=S \
 --key-schema AttributeName=id,KeyType=HASH \
 --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

