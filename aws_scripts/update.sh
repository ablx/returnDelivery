#!/usr/bin/env bash
mvn clean package shade:shade -DskipTests && \
aws s3 cp target/return-delivery-1.0.0.jar s3://ablx.lambda/return-delivery-1.0.0.jar && \
aws lambda update-function-code  --cli-input-json file://aws_scripts/json/update_function.json